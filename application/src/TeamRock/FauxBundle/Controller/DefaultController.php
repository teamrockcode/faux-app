<?php

namespace TeamRock\FauxBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use TeamRock\ContentBundle\Entity\News;
use TeamRock\ContentBundle\Form\NewsAddType;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="index")
     * @Template()
     * @param Request $request
     * @return array
     */
    public function indexAction(Request $request)
    {
        return [ ];
    }

    /**
     * @Route("/list", name="news_list")
     * @Template()
     * @param Request $request
     * @return array
     */
    public function listNewsAction(Request $request)
    {
        $newsRepository = $this->getDoctrine()->getRepository("TeamRockContentBundle:News");
        $news = $newsRepository->findAll();

        return [ 'news' => $news ];
    }

    /**
     * @Route("/add", name="news_add")
     * @Template()
     * @param Request $request
     * @return array
     */
    public function addNewsAction(Request $request)
    {
        $news = new News();
        $newsType = new NewsAddType();

        $newsForm = $this->createForm($newsType, $news);
        $newsForm->handleRequest($request);

        if ($newsForm->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($news);
            $entityManager->flush();

            return $this->redirect($this->generateUrl('news_list'));
        }

        return [ 'newsForm' => $newsForm->createView() ];
    }

    /**
     * @Route("/view/{identifier})", name="news_view")
     * @Template()
     */
    public function viewNewsAction($identifier)
    {
        $news = $this->getDoctrine()->getRepository("TeamRockContentBundle:News")->find($identifier);

        $news->onView();
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($news);
        $entityManager->flush();

        return [ 'news' => $news ];
    }
}
