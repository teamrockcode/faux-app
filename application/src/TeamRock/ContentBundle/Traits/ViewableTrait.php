<?php

namespace TeamRock\ContentBundle\Traits;

trait ViewableTrait
{
    /**
     * @var string
     *
     * @ORM\Column(type="integer")
     */
    protected $views = 0;

    /**
     * @return string
     */
    public function getViews()
    {
        return $this->views;
    }

    /**
     * @param string $views
     */
    public function setViews($views)
    {
        $this->views = $views;
    }

    public function onView()
    {
        ++$this->views;
    }
}
