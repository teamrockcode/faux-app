<?php

namespace TeamRock\ContentBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use TeamRock\ContentBundle\Interfaces\DeletableInterface;
use TeamRock\ContentBundle\Interfaces\ViewableInterface;
use TeamRock\ContentBundle\Traits\DeletableTrait;
use TeamRock\ContentBundle\Traits\ViewableTrait;

/**
 * News
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="TeamRock\ContentBundle\Repository\NewsRepository")
 */
class News implements ViewableInterface, DeletableInterface
{
    /**
     * @var guid
     * @ORM\Column(name="identifier", type="guid")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="UUID")
     */
    protected $identifier;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255)
     */
    protected $title;

    /*
     * Traits
     */
    use ViewableTrait;
    use DeletableTrait;

    /**
     * Get id
     *
     * @return integer
     */
    public function getIdentifier()
    {
        return $this->identifier;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return News
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

}
