<?php

namespace TeamRock\ContentBundle\Interfaces;

interface ViewableInterface
{
    public function onView();
}
